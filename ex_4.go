package main

import (
	"log"
	"time"
)

func main() {
	start := time.Now()
	var counter = 0
	for i := 1; i <= 16; i++ {
		counter++
		go func(thisCounter int) {
			log.Println("Start", thisCounter)
		}(counter)
	}
	elapsed := time.Since(start)
	log.Println(elapsed)
	time.Sleep(6 * time.Second)
}
