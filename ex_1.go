package main

import (
	"fmt"
	"log"
	"time"
)

func main() {
	start := time.Now()
	var counter = 0
	for i := 1; i <= 16; i++ {
		log.Println("Start", counter)
		counter++
	}
	elapsed := time.Since(start)
	fmt.Println(elapsed)
}
