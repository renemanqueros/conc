package main

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"
)

var Tasks []string
var mutex sync.Mutex

func main() {
	var wg sync.WaitGroup
	for i := 0; i < 32; i++ {
		wg.Add(1)
		go func(iteration int) {
			mutex.Lock()
			Tasks = append(Tasks, fmt.Sprintf("T%v", iteration))
			mutex.Unlock()

			duration := rand.Intn(5000)
			time.Sleep(time.Duration(duration) * time.Millisecond)

			wg.Done()
		}(i)
	}

	wg.Wait()
	log.Println(Tasks)
}
