package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Search func(query string) string

var Web = fakeSearch("web")
var Image = fakeSearch("image")
var Video = fakeSearch("video")

func fakeSearch(kind string) Search {
	return func(query string) string {
		time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
		return fmt.Sprintf("\n%s result for %q\n", kind, query)
	}
}

func searchEngine_1(query string) []string {
	var results []string
	results = append(results, Web(query))
	results = append(results, Image(query))
	results = append(results, Video(query))
	return results
}

func searchEngine_2(query string) []string {
	var results []string
	c := make(chan string)
	go func() {
		c <- Web(query)
	}()
	go func() {
		c <- Image(query)
	}()
	go func() {
		c <- Video(query)
	}()

	for i := 0; i < 3; i++ {
		result := <-c
		results = append(results, result)
	}
	return results
}

func searchEngine_3(query string) []string {
	var results []string
	c := make(chan string)
	go func() { c <- Web(query) }()
	go func() { c <- Image(query) }()
	go func() { c <- Video(query) }()

	timeout := time.After(80 * time.Millisecond)
	for i := 0; i < 3; i++ {
		select {
		case result := <-c:
			results = append(results, result)
		case <-timeout:
			fmt.Println("timed out")
			return results
		}
	}
	return results
}

func main() {
	rand.Seed(time.Now().UnixNano())
	time.Sleep(500 * time.Millisecond)

	start := time.Now()
	r1 := searchEngine_1("SE1")
	elapsed := time.Since(start)
	fmt.Println("SE1", elapsed, r1)

	start = time.Now()
	r2 := searchEngine_2("SE2")
	elapsed = time.Since(start)
	fmt.Println("SE2", elapsed, r2)

	start = time.Now()
	r3 := searchEngine_3("SE3")
	elapsed = time.Since(start)
	fmt.Println("SE3", elapsed, r3)
}
