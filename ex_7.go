package main

import (
	"fmt"
	"sync"
	"time"
)
var c = 0
var wg sync.WaitGroup

func main() {
	wg.Add(2)
	go func() {
		for i := 0; i < 8; i++ {
			c++
			time.Sleep(100 * time.Millisecond)
			fmt.Println("Thread 1: ", c)
		}
		wg.Done()
	}()

	go func() {
		for i := 0; i < 8; i++ {
			c++
			time.Sleep(100 * time.Millisecond)
			fmt.Println("Thread 2: ", c)
		}
		wg.Done()
	}()

	wg.Wait()
}
