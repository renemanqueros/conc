package main

import (
	"github.com/remeh/sizedwaitgroup"
	"log"
	"time"
)

func main() {
	start := time.Now()
	var counter = 0
	var swg = sizedwaitgroup.New(6)

	for i := 1; i <= 16; i++ {
		swg.Add()
		counter++
		go func(thisCounter int) {
			log.Println("Start", thisCounter)
			time.Sleep(1 * time.Second)
			swg.Done()
		}(counter)
	}
	swg.Wait()
	elapsed := time.Since(start)
	log.Println(elapsed)
}
