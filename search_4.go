package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Search func(query string) string

var Web = fakeSearch("web")
var Image = fakeSearch("image")
var Video = fakeSearch("video")

var Web1 = fakeSearch("web")
var Image1 = fakeSearch("image")
var Video1 = fakeSearch("video")

var Web2 = fakeSearch("web")
var Image2 = fakeSearch("image")
var Video2 = fakeSearch("video")

var Web3 = fakeSearch("web")
var Image3 = fakeSearch("image")
var Video3 = fakeSearch("video")

var Web4 = fakeSearch("web")
var Image4 = fakeSearch("image")
var Video4 = fakeSearch("video")

func fakeSearch(kind string) Search {
	return func(query string) string {
		time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
		return fmt.Sprintf("\n%s result for %q\n", kind, query)
	}
}

func searchEngine_1(query string) []string {
	var results []string
	results = append(results, Web(query))
	results = append(results, Image(query))
	results = append(results, Video(query))
	return results
}

func searchEngine_2(query string) []string {
	var results []string
	c := make(chan string)
	go func() {
		c <- Web(query)
	}()
	go func() {
		c <- Image(query)
	}()
	go func() {
		c <- Video(query)
	}()

	for i := 0; i < 3; i++ {
		result := <-c
		results = append(results, result)
	}
	return results
}

func searchEngine_3(query string) []string {
	var results []string
	c := make(chan string)
	go func() { c <- Web(query) }()
	go func() { c <- Image(query) }()
	go func() { c <- Video(query) }()

	timeout := time.After(80 * time.Millisecond)
	for i := 0; i < 3; i++ {
		select {
		case result := <-c:
			results = append(results, result)
		case <-timeout:
			fmt.Println("timed out")
			return results
		}
	}
	return results
}

func searchEngine_4(query string) []string {
	var results []string
	c := make(chan string)
	go func() { c <- First(query, Web1, Web2, Web3, Web4) }()
	go func() { c <- First(query, Image1, Image2, Image3, Image4) }()
	go func() { c <- First(query, Video1, Video2, Video3, Video4) }()
	timeout := time.After(80 * time.Millisecond)
	for i := 0; i < 3; i++ {
		select {
		case result := <-c:
			results = append(results, result)
		case <-timeout:
			fmt.Println("timed out")
			return results
		}
	}
	return results
}

func First(query string, replicas ...Search) string {
	c := make(chan string)
	searchReplica := func(i int) {
		c <- replicas[i](query)
	}
	for i := range replicas {
		go searchReplica(i)
	}
	return <-c
}

func main() {
	rand.Seed(time.Now().UnixNano())
	time.Sleep(500 * time.Millisecond)

	start := time.Now()
	r1 := searchEngine_1("SE1")
	elapsed := time.Since(start)
	fmt.Println("SE1", elapsed, r1)

	start = time.Now()
	r2 := searchEngine_2("SE2")
	elapsed = time.Since(start)
	fmt.Println("SE2", elapsed, r2)

	start = time.Now()
	r3 := searchEngine_3("SE3")
	elapsed = time.Since(start)
	fmt.Println("SE3", elapsed, r3)

	start = time.Now()
	r4 := searchEngine_4("SE4")
	elapsed = time.Since(start)
	fmt.Println("SE4", elapsed, r4)
}
