package main

import (
	"log"
)

func main() {
	c := make(chan int)

	go func() {
		for i := 0; i < 8; i++ {
			c <- 1
		}
	}()

	go func() {
		for i := 0; i < 8; i++ {
			c <- 1
		}
	}()

	go func() {
		for i := 0; i < 8; i++ {
			c <- 1
		}
	}()

	go func() {
		for i := 0; i < 8; i++ {
			c <- 1
		}
	}()

	{
		var counter = 0
		for counter < 32 {
			counter += <-c
			log.Println(counter)
		}
	}
}
