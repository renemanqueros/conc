package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Search func(query string) string

var Web = fakeSearch("web")
var Image = fakeSearch("image")
var Video = fakeSearch("video")

func fakeSearch(kind string) Search {
	return func(query string) string {
		time.Sleep(time.Duration(rand.Intn(180)) * time.Millisecond)
		return fmt.Sprintf("\n%s result for %q\n", kind, query)
	}
}

func searchEngine_1(query string) []string {
	var results []string
	results = append(results, Web(query))
	results = append(results, Image(query))
	results = append(results, Video(query))
	return results
}

func main() {
	rand.Seed(time.Now().UnixNano())
	time.Sleep(500 * time.Millisecond)

	start := time.Now()
	r := searchEngine_1("SE1")
	elapsed := time.Since(start)
	fmt.Println("SE1", elapsed, r)
}
